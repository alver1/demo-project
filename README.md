## OVERVIEW
The infrastructure contains 3 nodes:
•	jenkinsmaster has installed Jenkins for jobs runs. swarmmaster node is a jenkinsmaster's slave where job will run;
•	swarmmaster has installed Docker Enterprise Edition with Docker Swarm, Docker Compose, Universal Control Plane(UCP) and Docker Trusted Registry (DTR). This node is a MASTER NODE of the Docker Swarm cluster;
•	swarmslave1 has installed Docker Enterprise Edition with Docker Swarm and Docker Compose. This node is a WORKER NODE of the Docker Swarm cluster;
TASKi
1.	Create Vagrantfile which contains 3 nodes jenkinsmaster, swarmmaster and swarmslave1
2.	Create jenkins_install.sh for jenkinsmaster bootstrap and docker_install.sh for swarmmaster and swarmslave1  bootstrap (use Docker EE with one month trial license).
-	https://docs.docker.com/ee/end-to-end-install/ Deploy Enterprise Edition on Linux servers
-	https://docs.docker.com/install/linux/docker-ee/ubuntu/ Get Docker EE for CentOS
-	https://docs.docker.com/install/linux/docker-ee/centos/ Get Docker EE for Ubuntu
           Note: docker commands must run from sudo user without using sudo.
3.	When bootstrap process has been done need to install UCP and DTR on the swarmmaster node.
4.	Add swarmmaster as jenkinsmaster slave via ssh.
5.	Initialize swarmmaster as a master node of the Docker Swarm cluster.
6.	Add swarmslave1 as a worker node of the Docker Swarm cluster.
7.	Deploy the application as Docker stack app in the Docker Swarm cluster.
-	Create the Job which can build Java Spring App ( https://github.com/sqshq/PiggyMetrics ) 
-	Note: this java project is being built by Maven builder, so you need to create a Maven builder in the Jenkins and use compile before docker stack deploy.
-	https://github.com/sqshq/piggymetrics/blob/master/pom.xml
-	https://github.com/sqshq/piggymetrics/blob/master/docker-compose.yml
8.	Create a GitHub repo with Vagrantfile, docker_install.sh, jenkins_install.sh and README.md which contains a progress of each step + commands output and screens+outputs of Jenkins job and UCP of deployed app.

## PiggyMetrics deploying project
We are creating three virtual machines via Vagrant and Virtualbox.
```ruby
Vagrant.configure("2") do |config|

  config.vm.define "jenkinsmaster" do |jenkinsmaster|
    jenkinsmaster.vm.box = "ubuntu/bionic64"
    jenkinsmaster.vm.provision "shell", path: "jenkins_install.sh"
    jenkinsmaster.vm.network "public_network"
    jenkinsmaster.vm.hostname = "jenkinsmaster"
    jenkinsmaster.vm.provider "virtualbox" do |vb|
        vb.memory = "4096"
    end
  end

  config.vm.define "swarmmaster" do |swarmmaster|
    swarmmaster.vm.box = "ubuntu/bionic64"
    swarmmaster.vm.network "public_network"
    swarmmaster.vm.provision "shell", path: "docker_install.sh"
    swarmmaster.vm.hostname = "swarmmaster"
    swarmmaster.vm.provider "virtualbox" do |vb|
        vb.memory = "6096"
    end
  end

  config.vm.define "swarmslave1" do |swarmslave1|
    swarmslave1.vm.box = "ubuntu/bionic64"
    swarmslave1.vm.network "public_network"
    swarmslave1.vm.provision "shell", path: "docker_install.sh"
    swarmslave1.vm.hostname = "swarmslave1"
    swarmslave1.vm.provider "virtualbox" do |vb|
        vb.memory = "4096"
    end
  end
end
```
Output:
```
> vagrant up

Bringing machine 'jenkinsmaster' up with 'virtualbox' provider...
Bringing machine 'swarmmaster' up with 'virtualbox' provider...
Bringing machine 'swarmslave1' up with 'virtualbox' provider...

> vagrant status

Current machine states:

jenkinsmaster             running (virtualbox)
swarmmaster               running (virtualbox)
swarmslave1               running (virtualbox)
```
Jenkins has been succsesfully installed on 'jenkinsmaster' machine via script 'jenkins_install.sh':
```sh
#!/bin/bash
wget -q -O - https://pkg.jenkins.io/debian/jenkins.io.key | sudo apt-key add -
sudo sh -c 'echo deb https://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'
sudo apt update -y
sudo apt install -y openjdk-11-jdk
sudo apt install -y jenkins
```
![](https://i.screenshot.net/zr5njt6)
#
That is script 'docker_install.sh' for 'swarmmaster' and 'swarmslave' machines:
```sh
#!/bin/bash
wget https://storebits.docker.com/ee/ubuntu/sub-d9a040e4-c79c-4b64-ab87-713d50a2ac8b/ubuntu/dists/bionic/pool/stable-19.03/amd64/containerd.io_1.2.6-3_amd64.deb
wget https://storebits.docker.com/ee/ubuntu/sub-d9a040e4-c79c-4b64-ab87-713d50a2ac8b/ubuntu/dists/bionic/pool/stable-19.03/amd64/docker-ee-cli_19.03.2~3-0~ubuntu-bionic_amd64.deb
wget https://storebits.docker.com/ee/ubuntu/sub-d9a040e4-c79c-4b64-ab87-713d50a2ac8b/ubuntu/dists/bionic/pool/stable-19.03/amd64/docker-ee_19.03.2~3-0~ubuntu-bionic_amd64.deb
sudo dpkg -i containerd.io_1.2.6-3_amd64.deb docker-ee-cli_19.03.2~3-0~ubuntu-bionic_amd64.deb docker-ee_19.03.2~3-0~ubuntu-bionic_amd64.deb
sudo usermod -aG docker vagrant
sudo apt install -y openjdk-11-jdk
sudo reboot
```
[Installing UCP](https://docs.docker.com/ee/ucp/admin/install/):

```
$ docker container run --rm -it --name ucp \
-v /var/run/docker.sock:/var/run/docker.sock \
docker/ucp:3.2.1 install \
--pod-cidr 192.168.106/24 \
--host-address 192.168.104.55 \
--interactive
```
Output:

```
Unable to find image 'docker/ucp:3.2.1' locally
3.2.1: Pulling from docker/ucp
050382585609: Pull complete
39666cf3fdbb: Pull complete
51e8b2f21784: Pull complete
Digest: sha256:5732446035636c07f1b5f133827fc464da61990cf37c9ef850cff9012ec2bb0d
Status: Downloaded newer image for docker/ucp:3.2.1
INFO[0000] Your Docker daemon version 19.03.2, build c92ab06 (4.15.0-62-generic) is compatible
with UCP 3.2.1 (ee15519)
INFO[0000] Initializing New Docker Swarm
Admin Username: admin
Admin Password:
Confirm Admin Password:

WARN[0011] None of the Subject Alternative Names we'll be using in the UCP certificates
["swarmmaster"] contain a domain component. Your generated certs may fail TLS validation unless
you only use one of these shortnames or IP addresses to connect. You can use the --san flag to
add more aliases

You may enter additional aliases (SANs) now or press enter to proceed with the above list.

INFO[0051] Checking required ports for connectivity
INFO[0063] Checking required container images
INFO[0063] Pulling required images... (this may take a while)
...
INFO[0101] Step 35 of 35: [Wait for All Nodes to be Ready]
INFO[0106] All Installation Steps Completed
```
![](https://i.screenshot.net/lq18dc1)
#
[Installing DTR](https://docs.docker.com/ee/dtr/admin/install/):

```
curl -k https://192.168.104.55/ca > ucp-ca.pem \
docker run -it --rm docker/dtr install \
--ucp-node swarmmaster \
--ucp-username admin \
--replica-https-port 4443 \
--ucp-url https://192.168.104.55 \
--ucp-ca "$(cat ucp-ca.pem)"
```
![](https://i.screenshot.net/7jn2rie)
![](https://i.screenshot.net/52d0nup)
#
Adding slave node to Jenkins:
#
Creating ssh keys on 'jenkinsmaster' from user 'jenkins':
```
$ ssh-keygen
~/.ssh$ ls -la
total 19
drwx------  2 jenkins jenkins 4096 Sep 23 15:42 .
drwxr-xr-x 15 jenkins jenkins 4096 Sep 25 12:24 ..
-rw-------  1 jenkins jenkins 1675 Sep 23 15:40 id_rsa
-rw-r--r--  1 jenkins jenkins  403 Sep 23 15:40 id_rsa.pub
```
Creating user 'jenkins' on 'swarmmaster':
```
$ useradd -d /var/lib/jenkins jenkins
```
Putting public key from 'id_rsa.pub' on 'jenkinsmaster' to 'authorized_keys' on 'swarmmaster':
```
$ mkdir /var/lib/jenkins/.ssh/
$ vi /var/lib/jenkins/.ssh/authorized_keys
```

Manage Jenkins -> Manage Nodes -> New Node

![](https://i.screenshot.net/mkp0gs1)

![](https://i.screenshot.net/mm2zecd)

Adding slave node 'swarmslave1' to swarm:
```
vagrant@swarmmaster:~$ docker swarm join-token worker
To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-4cad103o3r4n7o3urk05477qilwvtoqo0c8u3sqhbvedgf6th2-d7ui6zogl0mmwnm6s1w483xaw 192.168.104.55:2377
```
```
vagrant@swarmslave1:~$ docker swarm join --token SWMTKN-1-4cad103o3r4n7o3urk05477qilwvtoqo0c8u3sqhbvedgf6th2-d7ui6zogl0mmwnm6s1w483xaw 192.168.104.55:2377
This node joined a swarm as a worker.
```

![](https://i.screenshot.net/v60z9fl)
```
$ docker node ls
ID                            HOSTNAME            STATUS              AVAILABILITY        MANAGER STATUS      ENGINE VERSION
quss0kg1rk4qrxnb0bvv0qryg *   swarmmaster         Ready               Active              Leader              19.03.2
fpy2qf1fa9kcrip5t6ge8faes     swarmslave1         Ready               Active                                  19.03.2
```
Creating Maven job:
Downloaded Maven plugin.
New item -> Maven project:
![](https://i.screenshot.net/xp13gtg)

Adding git repository's URL:
![](https://i.screenshot.net/v6o37fp)

Filling requaired settings:
![](https://i.screenshot.net/rrko9bp)
![](https://i.screenshot.net/3y1jmcv)

Building:
![](https://i.screenshot.net/n5l4ksr)

Checking on 'swarmmaster'
```
vagrant@swarmmaster:~$ docker stack ls
NAME                SERVICES            ORCHESTRATOR
PiggyMetrics        13                  Swarm
```
```
$ docker service ls | grep Piggy
```
![](https://i.screenshot.net/3170gt1)

Checking app:

![](https://i.screenshot.net/5d21gag)
![](https://i.screenshot.net/ee341cj)
![](https://i.screenshot.net/31djqtn)
![](https://i.screenshot.net/5ez9gcv)
![](https://i.screenshot.net/0r8e9ck)
![](https://i.screenshot.net/v82kpfv)

