docker container run --rm -it --name ucp \
-v /var/run/docker.sock:/var/run/docker.sock \
docker/ucp:3.2.1 install \
--pod-cidr 192.168.106/24 \
--host-address 192.168.104.55 \
--interactive
