#!/bin/bash
# Install Docker EE and Java
wget https://storebits.docker.com/ee/ubuntu/sub-d9a040e4-c79c-4b64-ab87-713d50a2ac8b/ubuntu/dists/bionic/pool/stable-19.03/amd64/containerd.io_1.2.6-3_amd64.deb
wget https://storebits.docker.com/ee/ubuntu/sub-d9a040e4-c79c-4b64-ab87-713d50a2ac8b/ubuntu/dists/bionic/pool/stable-19.03/amd64/docker-ee-cli_19.03.2~3-0~ubuntu-bionic_amd64.deb
wget https://storebits.docker.com/ee/ubuntu/sub-d9a040e4-c79c-4b64-ab87-713d50a2ac8b/ubuntu/dists/bionic/pool/stable-19.03/amd64/docker-ee_19.03.2~3-0~ubuntu-bionic_amd64.deb
sudo dpkg -i containerd.io_1.2.6-3_amd64.deb docker-ee-cli_19.03.2~3-0~ubuntu-bionic_amd64.deb docker-ee_19.03.2~3-0~ubuntu-bionic_amd64.deb
sudo usermod -aG docker vagrant
sudo apt-get update -y
sudo apt install -y openjdk-11-jdk
sudo reboot