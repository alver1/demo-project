#!/bin/bash
curl -k https://192.168.104.63/ca > cert.pem
docker run -it --rm docker/dtr install   --ucp-node swarmmaster  --ucp-username admin --ucp-url https://192.168.104.63 --ucp-ca "$(cat cert.pem)" -replica-https-port 8000
