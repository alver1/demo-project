#!/bin/bash
# Install Java and Jenkins
wget -q -O - https://pkg.jenkins.io/debian/jenkins.io.key | sudo apt-key add -
sudo sh -c 'echo deb https://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'
sudo apt-get update -y
sudo apt install -y openjdk-11-jdk
sudo apt-get install -y jenkins
sudo systemctl enable jenkins
